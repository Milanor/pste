#ifndef RECTANGLE_H_INCLUDED
#define RECTANGLE_H_INCLUDED

///cr�ation d'une classe rectangle
class Rectangle
{
    private :
         ///attributs en partie priv�e
        int m_longueur;
        int m_largeur;

    public :

        ///constructeurs
        Rectangle();
        Rectangle(int longueur,int largeur);
        ~Rectangle();

        ///m�thodes
        void perimetre(int *p);
        void aire(int *a);
        void affichage(int *p, int*a);

        ///getter
        int getlong() const;
        int getlarg() const;

};

#endif // RECTANGLE_H_INCLUDED
