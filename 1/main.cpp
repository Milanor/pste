#include <iostream>
#include "Rectangle.h"

using namespace std;

///constructeur par d�faut
Rectangle::Rectangle()
{
    m_longueur=10;
    m_largeur=20;
}

///constructeur surcharge
Rectangle::Rectangle(int longueur, int largeur) : m_longueur(longueur), m_largeur(largeur)
{

}

///m�thode pour calculer le p�rim�tre de la classe rectangle
void Rectangle::perimetre(int *p)
{
    *p= getlong()*2 + getlarg()*2;
}

///m�thode de la classe rectangle pour calculer l'aire
void Rectangle::aire(int *a)
{
    *a= getlong()*getlarg();
}

///m�thode de la classe rectangle pour afficher les caract�ristiques du rectangle
void Rectangle::affichage(int *p,int*a)
{
    cout << "Les dimensions du rectangle sont : "<< getlong() << " et " << getlarg() <<endl;
    cout << "Le perimetre du rectangle est : " << *p << endl;
    cout << "L'aire du rectangle est : " << *a << endl << endl;
}

///get la longueur
int Rectangle ::getlong() const
{
    return m_longueur;
}
///get la largeur
int Rectangle ::getlarg() const
{
    return m_largeur;
}

///destructeur
Rectangle::~Rectangle()
{

}

int main()
{
    int a,p;
    int* aire=&a;
    int* perimetre=&p;

    Rectangle test;
    Rectangle test1(30,4);
    test.perimetre(perimetre);
    test.aire(aire);
    test.affichage(perimetre,aire);
    test1.perimetre(perimetre);
    test1.aire(aire);
    test1.affichage(perimetre,aire);
    return 0;
}
